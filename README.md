## **Install**
Clone this repository and then run:

    npm install
    pip3 install -r requirements.txt

Once everything is installed run it with:

    npm run linkedin [false]
    
Being the last argument `false` if you don't want to run it in headless mode.
