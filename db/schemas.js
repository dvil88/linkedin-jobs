const mongoose = require('mongoose');

const schemas = {
	'appliedJobs': new mongoose.Schema({
		id: { type: String, index: true },
		title: String,
		url: String,
		company: String,
		location: String,
		time: String,
		jobDescription: String,
		language: String,
		jobDescriptionStemmed: Array,
		processStatus: String,
	}, {
		versionKey: false // You should be aware of the outcome after set to false
	}),
	'pendingJobs': new mongoose.Schema({
		id: { type: String, index: true },
		title: String,
		url: String,
		company: String,
		companyUrl: String,
		location: String,
		time: String,
		jobDescription: String,
		language: String,
		simple: Boolean,
		processStatus: String,
	}, {
		versionKey: false // You should be aware of the outcome after set to false
	}),
};
module.exports = schemas;