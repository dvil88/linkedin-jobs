const fs = require('fs');
const puppeteer = require('puppeteer');
const Config = require('conf');
const path = require('path');
const prompt = require('prompt-sync')({
    sigint: false
});
const mongoose = require('mongoose');
const logger = require('./libs/logger.js');


if( process.argv.length >= 3 ){
	bot = process.argv[2];
} else {
	logger.error('You have to specify the robot to use: npm start [robot] [headless]');
	process.exit();
}

var headlessMode = true;
if( process.argv.length == 4 ){
	headlessMode = false;
}


// Load config set
const config = new Config();
const configData = config.get();

var mongoConfig = configData.mongo;

if( !mongoConfig ){
	logger.error('Error, you have to configure the DB');
	var host = prompt(' * Mongo host [localhost]: ', {value: 'localhost'});
	var port = prompt(' * Mongo port [27017]: ', {value: '27017'});
	do{
		var user = prompt(' * Mongo user: ');
	} while( user == null );
	var pass = prompt(' * Mongo password: ', {echo: '*'});
	var database = prompt(' * Mongo database: ');


	conf = {
		host: host,
		port: port,
		user: user,
		pass: pass,
		database: database
	};
	mongoConfig = conf;
	config.set({mongo: conf});
}


puppeteer.launch({
	args: ["--no-sandbox"],
	ignoreHTTPSErrors: true,
	headless: headlessMode,
	defaultViewport: null,
	userDataDir: '/tmp/linkedinTest/',
	pipe: true,
}).then(async browser =>{
	logger.debug(bot,'::','init');

	var pages = await browser.pages();
	var page = pages[0];

	try{
		// logger.debug(path.resolve('./bots/' + bot + '.js'));
		var botName = require(path.resolve('./bots/' + bot + '.js'));
		var robot = new botName();

		page.on('close', () => {
			logger.debug(bot,'::','page closed');
		});
	} catch(error){
		logger.warn(bot,'::','Bot not found');
		logger.error(bot,'::',error);
		return browser;
	}

	var scrapper = await robot.run(browser, page);

	if( scrapper.error === true ){
		logger.error(bot,'::',scrapper.errorDescription);
	} else {
		logger.info(bot,'::','Bot finished its job');
	}
	
	return browser;
}).then(async browser => {
	logger.debug('close browser');
	await browser.close();
}).then(() => {
	logger.debug('exit');
	process.exit();
});
