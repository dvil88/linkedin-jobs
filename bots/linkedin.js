const logger = require('../libs/logger.js');
const schemas = require('../db/schemas.js');

const {PythonShell} = require('python-shell');
const Config = require('conf');
const prompt = require('prompt-sync')({
    sigint: false
});

const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const LanguageDetect = require('languagedetect');


// Load config set
const config = new Config();
const configData = config.get();
const lngDetector = new LanguageDetect();

// var mongoConfig = configData.mongo;

var _this;

class linkedin {
	constructor(){
		this.botName = 'linkedin';

		var mongoConfig = configData.mongo;
		var mongoServer = 'mongodb://' + mongoConfig.user + ':' + mongoConfig.pass + '@' + mongoConfig.host + ':' + mongoConfig.port + '/' + mongoConfig.database + '?authSource=admin';
		mongoose.connect(mongoServer, { useNewUrlParser: true, useUnifiedTopology: true });

		this.appliedJobsModel = mongoose.model('applied_jobs', schemas.appliedJobs);
		this.pendingJobsModel = mongoose.model('pending_jobs', schemas.pendingJobs);

		this.pageSniffed = false;
	}

	async run(browser, page){
		this.browser = browser;
		this.page = page;

		_this = this;

		await this.page.setRequestInterception(true);
		this.page.on('request', this.interceptedRequest);
		this.page.on('response', this.response);


		this.userConfig = configData.linkedInUser;

		if( !this.userConfig ){
			logger.error('Error, you have to configure the LinkedIn user details');
			do{
				var user = prompt(' * LinkedIn user: ');
			} while( user == null );
			var pass = prompt(' * LinkedIn password: ', {echo: '*'});

			var conf = {
				user: user,
				pass: pass,
			};
			this.userConfig = conf;
			config.set({linkedInUser: conf});
		}

		logger.info('Running on user \'' + this.userConfig.user + '\'');



		var working = await this.page.goto('https://www.linkedin.com/', {
			waitUntil: 'networkidle2',
		}).catch(() => {
			return false;
		});

		// Check login
		if( await this.page.$('div[class*="profile-rail-card__actor-link"]').catch(() => { }) === null ){
			await this.doLogin();
		}
		

		var userName = await this.page.evaluate(() => {
			return document.querySelector('div[class*="profile-rail-card__actor-link"]').textContent.trim();
		});
		logger.info('User:', userName);

		do{
			var quit = await this.selectAction();
		} while( !quit );

		return { error: false };
	}

	async selectAction(){
		console.log('\n 1 - Read applied jobs\n 2 - Generate AI model\n 3 - Search for jobs\n 4 - Apply to jobs automatically\n\n Q - Exit');
		var option = prompt(' Option: ');

		switch( option.toUpperCase() ){
			case '1':
				await this.readJobs();
				return false;
			case '2':
				await this.generateModel();
				return false;
			case '3':
				await this.searchJobs();
				return false;
			case '4':
				await this.applyJobs();
				return false;
			case 'Q':
				return true;
			default:
				var option = prompt(' Option: ');
				break;
		}
	}

	async doLogin(){
		var userInput = await this.page.waitForSelector('input#session_key', {visible: true}).catch(() => { return false; });
		if( !userInput ){ return this.error('No user input'); }

		var passInput = await this.page.waitForSelector('input#session_password', {visible: true}).catch(() => { return false; });
		if( !passInput ){ return this.error('No password input'); }

		// Fill user and password
		await this.page.focus('input#session_key');
		await this.page.type('input#session_key', this.userConfig.user, {delay: 100});
		await this.page.focus('input#session_password');
		await this.page.type('input#session_password', this.userConfig.pass, {delay: 100});

		// Login button
		var loginButton = await this.page.waitForSelector('button[class="sign-in-form__submit-button"]', {visible: true}).catch(() => { return false; });
		if( !loginButton ){ return this.error('No login button'); }

		await this.page.click('button[class="sign-in-form__submit-button"]');

		// Is it asking for phone number?
		var phoneNumber = await this.page.waitForSelector('input#phone-input', {visible: true, timeout: 5000}).catch(() => { return false; });
		if( phoneNumber ){
			await this.page.waitFor('div.cp-challenge button[class="secondary-action"]', {visible: true});
			await this.page.click('div.cp-challenge button[class="secondary-action"]');
		}
	}

	async readJobs(){
		logger.debug('readJobs');

		var jobsLink = await this.page.waitForSelector('li[class="global-nav__primary-item"] a[data-link-to="jobs"]', {visible: true}).catch(() => { return false; });
		if( !jobsLink ){ return logger.error('No jobs link'); }

		await this.page.click('li[class="global-nav__primary-item"] a[data-link-to="jobs"]');

		var myjobsLink = await this.page.waitForSelector('a[data-control-name="jobshome_nav_my_jobs"]', {visible: true}).catch(() => { return false; });
		if( !myjobsLink ){ return logger.error('No my jobs link'); }

		await this.page.click('a[data-control-name="jobshome_nav_my_jobs"]');

		var appliedJobsLink = await this.page.waitForSelector('button[aria-label="Solicitados"]', {visible: true}).catch(() => { return false; });
		if( !appliedJobsLink ){ return logger.error('No applied jobs link'); }

		await this.page.click('button[aria-label="Solicitados"]');

		await this.page.waitForSelector('li[class*="reusable-search__result-container"]', {visible: true}).catch(() => { return false; });

		while( true ){
			var data = await this.page.evaluate(() => {
				var _jobs = [];
				var _nextPage = false;

				var elements = document.querySelectorAll('li[class*="reusable-search__result-container"]');
				for( const element of elements ){
					var _title = element.querySelector('span[class*="entity-result__title"] a.app-aware-link').textContent.trim();
					var _url = element.querySelector('span[class*="entity-result__title"] a.app-aware-link').href.trim();
					var _company = element.querySelector('div[class*="entity-result__primary-subtitle"]').textContent.trim();
					var _location = element.querySelector('div[class*="entity-result__secondary-subtitle"]').textContent.trim();
					var _time = element.querySelector('span[class*="entity-result__simple-insight-text"]').textContent.trim();

					var _id = _url.match(/\/view\/([0-9]+)/)[1];

					var data = {
						id: _id,
						title: _title,
						url: _url,
						company: _company,
						location: _location,
						time: _time
					};
					_jobs.push(data);

					_nextPage = document.querySelector('button[class*="artdeco-pagination__button--next"]:not([disabled=""])') ? true : false;
				}

				return {jobs: _jobs, nextPage: _nextPage};
			});


			for( const job of data.jobs ){
				var jobOB = await this.appliedJobsModel.findOne({ 'id': job.id });
				if( !jobOB ){
					job.processStatus = 'awaiting';

					const jobOB = new this.appliedJobsModel(job);
					jobOB.save();
				}
			}

			if( data.nextPage ){
				var nextButton = await this.page.waitForSelector('button[class*="artdeco-pagination__button--next"]', {visible: true, timeout: 1000}).catch(() => { return false; });
				if( !nextButton ){ return logger.error('No next button'); }
				await this.page.click('button[class*="artdeco-pagination__button--next"]');
				await this.page.waitForTimeout(1000);
				continue;
			}
			break;
		}

		while( this.appliedJob = await this.appliedJobsModel.findOneAndUpdate({ 'processStatus': 'awaiting' }, { 'processStatus': 'working' }, { returnOriginal: false, upsert: false }) ){
			console.log(this.appliedJob);

			await this.processJobpage(this.appliedJob, this.appliedJobsModel);
		}
	}
	
	async generateModel(){
		logger.debug('generateModel');

		/*
		const natural = require('natural');
		natural.PorterStemmer.attach();

		// Generate stemmed strings and detect language
		// for await( const appliedJobOB of this.appliedJobsModel.find({jobDescriptionStemmed: {$exists: false}, processStatus: 'processed'}, null, {$sort: { _id: 1 }}) ){
		
		for await( const appliedJobOB of this.appliedJobsModel.find({ processStatus: 'processed'}, null, {$sort: { _id: 1 }}) ){
			if( !appliedJobOB.jobDescription ){ continue; }

			var language = lngDetector.detect(appliedJobOB.jobDescription, 1)[0];
			var stemmed = appliedJobOB.jobDescription.tokenizeAndStem();

			try {
				await this.appliedJobsModel.findByIdAndUpdate(appliedJobOB._id, {jobDescriptionStemmed: stemmed, language: language[0]}, { returnOriginal: false, upsert: false }); 
			} catch(err){
				console.log('err', err)
			}
		}
		*/

		await this.unPythonScript('bots/python/', ['train'], 'nlp_jobs.py');
		

		/*
		const tokenizer = new natural.WordTokenizer();
		const stemmer = new natural.PorterStemmer();

		var words = tokenizer.tokenize(str);

		for( const word of words ){

		}
		*/

		// var stem = stemmer.stem("We are looking for two graduates in engineering with at least 4 years of experience in development; with an interest in design and who want to join a startup with the best work environment. The IT team is growing, and we are looking for you!\nSomething important: humor is essential for this super-funny-team!\n\n\nFirst things first... top project details!\n- Company / team: We are talking about an electronics company focused on magnetic components; a company that has not stopped growing and that is clear about one thing: there are no limits! With a team of more than 30 people, you will join an international project in continuous innovation.\n- Values: If you are a restless person... this is your offer! Proactive team, with enthusiasm and drive, who loves to try new things, with a techie mentality and that is committed to autonomy.\n- Day to day: In your day-to-day, you will work with an IT team of 6 people (8 soon! Yes, we are hiring!) that is very close to the other departments: we want you to learn as much as possible and more. To give you an idea... a hurricane is just around the corner!\n\n\nYour mission 🚀\nHelp to develop the company's digital product.\nBuild a platform that supports the business.\nWork closely with the product team.\nSupport in innovation and design.\nAs a member of IT team, help to continue growing.\nParticipate in the innovation and development of the different projects in which the team works.\n\n\nPerks, my friend!\nCompetitive salary.\nFlexible schedule and remote work occasionally.\nGood working environment.\nAfter works activities.\n\n\nIf this is you – holy cow! 🐄\nDemonstrable experience in web development.\nAt least, 4 years experience working with PHP, Laravel or JS languages.\nExperience working with MariaDB.\nKnowledge working with CSS3.\nFirst level of English\nGood teammate.\n\n\nIf this is also you – JACKPOT! 🍒🍒🍒\nDemonstrable experience working with jQuery.\nKnowledge of AWS\nIf you want to join a growing project, where innovation and the desire to do things differently prevail... do not hesitate to apply!\n\n\nLet’s have a chat and GetWith us! 😊");
		// console.log(stem);
	}
	
	async searchJobs(){
		logger.debug('searchJobs');

		var result = await this.unPythonScript('bots/python/', ['keywords'], 'nlp_jobs.py');
		var keywords = JSON.parse(result);


		for( const keyword in keywords ){
break;
			logger.debug(keyword);


			// Clink in jobs menu
			var jobsLink = await this.page.waitForSelector('li[class="global-nav__primary-item"] a[data-link-to="jobs"]', {visible: true}).catch(() => { return false; });
			if( !jobsLink ){ return logger.error('No jobs link'); }

			await this.page.click('li[class="global-nav__primary-item"] a[data-link-to="jobs"]');


			var searchInput = await this.page.waitForSelector('input[id*="jobs-search-box-keyword-id"]', {visible: true}).catch(() => { return false; })
			if( !searchInput ){ return logger.error('No job search input'); }

			await this.page.focus('input[id*="jobs-search-box-keyword-id"]');
			await this.page.waitForTimeout(500);
			await this.page.type('input[id*="jobs-search-box-keyword-id"]', keyword, {delay: 100});

			await this.page.waitForTimeout(500);

			var locationInput = await this.page.waitForSelector('input[id*="jobs-search-box-location-id"]', {visible: true}).catch(() => { return false; })
			if( !locationInput ){ return logger.error('No job location search input'); }

			await this.page.focus('input[id*="jobs-search-box-location-id"]');
			await this.page.type('input[id*="jobs-search-box-location-id"]', 'Madrid', {delay: 100});

			await this.page.waitForTimeout(500);


			var searchButton = await this.page.waitForSelector('button[class^="jobs-search-box__submit-button"]', {visible: true}).catch(() => { return false; });
			if( !searchButton ){ return logger.error('No search button'); }

			// Search
			await this.page.click('button[class^="jobs-search-box__submit-button"]');


			// Filter
			/*
			var filter = await this.page.waitForSelector('button[aria-label*="Funcionalidades de LinkedIn"][class*="search-reusables__filter-pill-button"]', {visible: true, timeout: 5000}).catch(() => { return false; });
			if( !filter ){ return logger.error('No search dropdown'); }
			await this.page.click('button[aria-label*="Funcionalidades de LinkedIn"][class*="search-reusables__filter-pill-button"]');

			var simpleFilter = await this.page.waitForSelector('input[id="linkedinFeatures-f_AL"]', {visible: true, timeout: 5000}).catch(() => { return false; });
			if( !simpleFilter ){ return logger.error('No simple filter'); }
			await this.page.click('input[id="linkedinFeatures-f_AL"]');
			
			await this.page.waitForTimeout(500);

			await this.page.evaluate(() => {
				document.querySelectorAll('[data-control-name="filter_show_results"]')[document.querySelectorAll('[data-control-name="filter_show_results"]').length - 1].click();
			});
			*/

			await this.page.waitForTimeout(1000);


			var searchResults = await this.page.waitForSelector('li[class^="jobs-search-results__list-item"]', {visible: true}).catch(() => { return false; });
			if( !searchResults ){ return logger.error('No search results'); }

			var paginator = await this.page.waitForSelector('ul[class*="artdeco-pagination__pages"] li:last-child', {visible: true}).catch(() => { return false; });
			if( !paginator ){ return logger.error('No paginator'); }

			var totalPages = await this.page.evaluate(() => {
				return document.querySelector('ul[class*="artdeco-pagination__pages"] li:last-child').getAttribute('data-test-pagination-page-btn');
			});

			for( var page = 1; page <= totalPages; page++ ){
				logger.debug('Page:', page);


				await this.page.evaluate(async () => {
					await new Promise((resolve, reject) => {
						var scrolled = 0;
						var timer = setInterval(() => {
							var scrollHeight = document.body.scrollHeight;
							window.scrollBy(0, window.innerHeight);
							scrolled += window.innerHeight;

							if( scrolled >= scrollHeight ){
								clearInterval(timer);
								resolve();
							}
						}, 500);
					})
				});


				// Get results and check similarity
				var data = await this.page.evaluate(() => {
					var _jobs = [];
					// jobs-search-results__list-item occludable-update p0 relative ember-view
					var elements = document.querySelectorAll('li[class^="jobs-search-results__list-item"]');
					for( const e of elements ){
						if( !e.querySelector('a[class*="job-card-list__title"]') ){ continue; }

						var _title = e.querySelector('a[class*="job-card-list__title"]').textContent.trim();
						var _url = e.querySelector('a[class*="job-card-list__title"]').href.trim();
						var _company = e.querySelector('div[class^="artdeco-entity-lockup__subtitle"] a') ? e.querySelector('div[class^="artdeco-entity-lockup__subtitle"] a').textContent.trim() : '';
						var _companyUrl = e.querySelector('div[class^="artdeco-entity-lockup__subtitle"] a') ? e.querySelector('div[class^="artdeco-entity-lockup__subtitle"] a').href.trim() : '';
						var _location = e.querySelector('div[class^="artdeco-entity-lockup__caption"]').textContent.trim();
						var _time = e.querySelector('li[class*="job-card-container__listed-time"] time').getAttribute('datetime');
						var _simple = e.querySelector('li[class*="job-card-container__apply-method"]') ? true : false;

						var _id = _url.match(/\/view\/([0-9]+)/)[1];

						var data = {
							id: _id,
							title: _title,
							url: _url,
							company: _company,
							companyUrl: _companyUrl,
							location: _location,
							time: _time,
							simple: _simple
						};
						_jobs.push(data);
					}

					return {jobs: _jobs};
				});

				logger.debug('jobs length:', data.jobs.length);

				for( const job of data.jobs ){
					var jobOB = await this.pendingJobsModel.findOne({ 'id': job.id });
					if( !jobOB ){
						job.processStatus = 'awaiting';

						const jobOB = new this.pendingJobsModel(job);
						await jobOB.save();
					}
				}
				
				await this.page.evaluate((page) => {
					if( document.querySelector('ul[class*="artdeco-pagination__pages"] li[data-test-pagination-page-btn="'+page+'"]').nextElementSibling ){
						document.querySelector('ul[class*="artdeco-pagination__pages"] li[data-test-pagination-page-btn="'+page+'"]').nextElementSibling.firstElementChild.click();
					}
				}, page);

				await this.page.waitForTimeout(1000);
			}
		}

		// Check pending jobs
		while( this.pendingJob = await this.pendingJobsModel.findOneAndUpdate({ 'processStatus': 'awaiting', 'simple': true }, { 'processStatus': 'working' }, { returnOriginal: false, upsert: false }) ){
			console.log(this.pendingJob);

			await this.processJobpage(this.pendingJob, this.pendingJobsModel);
		}
	}
	
	async applyJobs(){
		logger.debug('applyJobs');

		var result = await this.unPythonScript('bots/python/', ['similar'], 'nlp_jobs.py');
		result = JSON.parse(result);

		for( const jobResult of result ){
			if( jobResult.similarity >= 30 ){
				var pendingJob = await this.pendingJobsModel.findOne({'_id': jobResult._id});
				logger.info(pendingJob.id+ ' :: '+pendingJob.title);

				var jobPage = await this.browser.newPage();
				await jobPage.goto(pendingJob.url, {
					waitUntil: 'networkidle2',
				});

				if( await jobPage.$('div[class^="jobs-s-apply__applied-date"]') ){
					await this.pendingJobsModel.findByIdAndUpdate(pendingJob._id, {'processStatus': 'applied'}, { returnOriginal: false, upsert: false });
					
					jobPage.close();
					logger.info('Job already applied');
					continue;
				}

				var applyButton = await jobPage.waitForSelector('button[class*="jobs-apply-button"]', {visible: true}).catch(() => { return false; });
				if( !applyButton ){ jobPage.close(); return logger.error('No apply button'); }

				await jobPage.click('button[class*="jobs-apply-button"]');

				while( true ){
					var title = await jobPage.evaluate(() => {
						return document.querySelector('div[class*="jobs-easy-apply-content"] h3:not([class*="attachment-filename"])').textContent.trim();
					});
					
					logger.debug(title);

					/*
					switch( title ){
						case 'Resume':
						case 'Currículum':
							break;
						case 'Información de contacto':
						case 'Preguntas adicionales':
						case 'Additional Questions':
						case 'Home address':
							var pending = await jobPage.evaluate(() => {
								var _pending = [];

								var _selects = document.querySelectorAll('div[class^="jobs-easy-apply-form-section__grouping"] select');
								var _inputs = document.querySelectorAll('div[class^="jobs-easy-apply-form-section__grouping"] input');

								// TODO fill info automatically
								for( const _select of _selects ){
									if( !_select.value ){
										_pending.push(_select.id);
									}
								}

								var _radios = {};
								for( const _input of _inputs ){
									switch( _input.type ){
										case 'text':
											if( _input.value == '' ){
												var question = document.querySelector('label[for="' + _input.id + '"] span').textContent.trim();
												_pending.push({type: 'text', value: _input.id, question: question});
											}
											break;
										case 'radio':
											if( !_radios[_input.name] ){ _radios[_input.name] = []; }
											_radios[_input.name].push(_input.value);
											break;
										case 'checkbox':
											break;
									}
								}

								for( const radioName in _radios ){
									if( !document.querySelector('input[name="' + radioName + '"]').checked ){
										var question = document.querySelector('legend[for="' + radioName + '"] span').textContent.trim();
										_pending.push({type: 'radio', value: radioName, question: question, options: _radios[radioName]});
									}
								}

								return _pending;
							});

							if( pending.length ){
								for( let pend of pending ){
									if( pend.options ){
										console.log(pend.question);
										for( const k in pend.options ){
											console.log(k + ' - ' + pend.options[k]);
										}
										var response = prompt('Option: ');
										pend.response = response;

										await jobPage.click('[name="' + pend.value + '"][value="' + pend.options[parseInt(response)] + '"]')
									} else {
										var response = prompt(pend.question + " ");
										pend.response = response;

										await jobPage.type('[id="' + pend.value + '"]', response, {delay: 100});
									}
								}
							}
							break;
						case 'Revisar tu solicitud':
						default:
							break;
					}
					*/

					var pending = await jobPage.evaluate(() => {
						var _pending = [];

						var _selects = document.querySelectorAll('div[class^="jobs-easy-apply-form-section__grouping"] select');
						var _inputs = document.querySelectorAll('div[class^="jobs-easy-apply-form-section__grouping"] input');

						// TODO fill info automatically
						for( const _select of _selects ){
							if( !_select.value ){
								_pending.push(_select.id);
							}
						}

						var _radios = {};
						for( const _input of _inputs ){
							switch( _input.type ){
								case 'text':
									if( _input.value == '' ){
										var question = document.querySelector('label[for="' + _input.id + '"] span').textContent.trim();
										_pending.push({type: 'text', value: _input.id, question: question});
									}
									break;
								case 'radio':
									if( !_radios[_input.name] ){ _radios[_input.name] = []; }
									_radios[_input.name].push(_input.value);
									break;
								case 'checkbox':
									break;
							}
						}

						for( const radioName in _radios ){
							if( !document.querySelector('input[name="' + radioName + '"]').checked ){
								var question = document.querySelector('legend[for="' + radioName + '"] span').textContent.trim();
								_pending.push({type: 'radio', value: radioName, question: question, options: _radios[radioName]});
							}
						}

						return _pending;
					});

					if( pending.length ){
						for( let pend of pending ){
							if( pend.options ){
								console.log(pend.question);
								for( const k in pend.options ){
									console.log(k + ' - ' + pend.options[k]);
								}
								var response = prompt('Option: ');
								pend.response = response;

								await jobPage.click('[name="' + pend.value + '"][value="' + pend.options[parseInt(response)] + '"]')
							} else {
								var response = prompt(pend.question + " ");
								pend.response = response;

								await jobPage.type('[id="' + pend.value + '"]', response, {delay: 100});
							}
						}
					}


					if( await jobPage.$('div[class*="jobs-easy-apply-content"] footer button[data-control-name="continue_unify"]') ){
						var nextButton = await jobPage.waitForSelector('div[class*="jobs-easy-apply-content"] footer button[data-control-name="continue_unify"]', {visible: true, timeout: 1000}).catch(() => { return false; });
						if( nextButton ){
							await jobPage.click('div[class*="jobs-easy-apply-content"] footer button[data-control-name="continue_unify"]', {delay: 200});
							continue;
						} else {
							logger.error('No next button');
						}
					} else if( await jobPage.$('div[class*="jobs-easy-apply-content"] footer button[data-control-name="review_unify"]') ){
						var finishButton = await jobPage.waitForSelector('div[class*="jobs-easy-apply-content"] footer button[data-control-name="review_unify"]', {visible: true, timeout: 1000}).catch(() => { return false; });
						if( finishButton ){
							await jobPage.click('div[class*="jobs-easy-apply-content"] footer button[data-control-name="review_unify"]', {delay: 200});
							continue;
						} else {
							logger.error('No review button');
						}
					} else if( await jobPage.$('div[class*="jobs-easy-apply-content"] footer button[data-control-name="submit_unify"]') ){
						var finishButton = await jobPage.waitForSelector('div[class*="jobs-easy-apply-content"] footer button[data-control-name="submit_unify"]', {visible: true, timeout: 1000}).catch(() => { return false; });
						if( finishButton ){
							await jobPage.click('div[class*="jobs-easy-apply-content"] footer button[data-control-name="submit_unify"]', {delay: 200});
							break;
						} else {
							logger.error('No submit button');
						}
					} else {
						return logger.error('Houston, we have a problem, I don\'t know how to contionue.');
					}

					

					await jobPage.waitForTimeout(2000);
				}

				await this.pendingJobsModel.findByIdAndUpdate(pendingJob._id, {'processStatus': 'applied'}, { returnOriginal: false, upsert: false }); 
				jobPage.close();
			}
		}
	}

	cleanText(text){
		text = text.replace(/<[^>]*>?/gm, "\n");
		text = text.replace(/\n\n/gm, "\n");
		text = text.replace(/&nbsp;/gm, ' ');
		text = text.trim();
		return text;
	}

	async unPythonScript(scriptPath, args, pythonFile){
		let options = {
			mode: 'text',
			pythonOptions: [], 
			scriptPath: scriptPath,
			args: args,
		};

		return new Promise((resolve,reject) =>{
			try{
				PythonShell.run(pythonFile, options, function(err, results) {
					if( err ){
						console.log(err);
					}
					// results is an array consisting of messages collected during execution
					resolve(results[0]);
				}); 
			}
			catch{
				console.log('error running python code')
				reject();
			}
		})
	}

	async processJobpage(job, model){
		var jobPage = await this.browser.newPage();
		await jobPage.goto(job.url, {
			waitUntil: 'networkidle2',
		});


		var jobDescription = await jobPage.waitForSelector('div[class*="jobs-description-content__text"] span', {visible: true, timeout: 5000}).catch(() => {});
		if( !jobDescription ){ 
			logger.error('No job description');				

			var update = {
				processStatus: 'noinfo'
			};
			try {
				await model.findByIdAndUpdate(job._id, update, { returnOriginal: false, upsert: false }); 
			} catch(err){
				console.log('err', err)
			}

			await jobPage.close();
			return;
		}


		var _jobDescription = await jobPage.evaluate(() => {
			return document.querySelector('div[class*="jobs-description-content__text"] span').innerHTML;
		});
		_jobDescription = this.cleanText(_jobDescription);


		var language = lngDetector.detect(_jobDescription, 1)[0];


		var update = {
			language: language[0],
			jobDescription: _jobDescription,
			processStatus: 'processed'
		};
		try {
			await model.findByIdAndUpdate(job._id, update, { returnOriginal: false, upsert: false }); 
		} catch(err){
			console.log('err', err)
		}

		await jobPage.close();
	}

	async error(errorDescription){
		logger.info(this.botName,'::','close page');
		// await this.page.close();
		return { error: true, errorDescription: errorDescription };
	}


	async interceptedRequest(interceptedRequest){
		return interceptedRequest.continue();
		/*
		if( interceptedRequest.url().match('\/nwsAjax\/Plate\?') && false ){
			const options = {
				uri: interceptedRequest.url(),
				method: interceptedRequest.method(),
				headers: interceptedRequest.headers(),
				body: interceptedRequest.postData(),
				encoding: null
			};

			return request(options, function(err, resp, body) {
				// Abort request on error
				if( err ){
					logger.error(bot,'::', 'Unable to call', options.uri, err);
					return interceptedRequest.abort('connectionrefused');
				}

				if( resp.statusCode != 200 ){
					_this.vin = { error: true, action: 'sleep' };
					logger.error(bot,'::', resp.statusCode, resp.statusMessage);
					_this.pageSniffed = true;
					return interceptedRequest.continue();
				}

				let buffer = new Buffer.from(body, 'utf8');
				var json = JSON.parse(buffer.toString());

				if( json.captchaValidation ){
					return {error: true, action: 'kill'}
				}
				var vin = json.vin;

				console.log(buffer.toString());

				_this.vin = vin;
				_this.pageSniffed = true;

				return interceptedRequest.continue();
			});

		} else {

			return interceptedRequest.continue();
		}
		*/
	}

	async response(response){
		// logger.debug('RESPONSE',response.url());
		/*
		if( response.url().match('\/nwsAjax\/Plate\?') ){
				// console.log('response',response.text());
			response.text().then(function(textBody) {
				var json = JSON.parse(textBody);
				console.log('json', json)
			});
		}
		*/
	}

	async waitFor(testFx, timeOutMillis, page) {
		var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000,
		start = new Date().getTime(),
		condition = false;

		do{
			if( (new Date().getTime() - start < maxtimeOutMillis) && !condition ){
				// If not time-out yet and condition not yet fulfilled
				condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
			} else {
				if( !condition ){
					// If condition still not fulfilled (timeout but condition is 'false')
					// logger.debug(this.botName,'::', "'waitFor()' timeout " + timeOutMillis);

					return false;
				} else {
					// Condition fulfilled (timeout and/or condition is 'true')
					// logger.debug(this.botName,'::', "'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");

					return true;
				}
			}

			await this.page.waitFor(250);
		} while( (new Date().getTime() - start < maxtimeOutMillis) );

		return false;
	}
}
module.exports = linkedin;