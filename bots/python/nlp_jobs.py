import sys
import json
import signal

import model

from pathlib import Path
from pymongo import MongoClient


with open(str(Path.home())+'/.config/linkedinjobsbot-nodejs/config.json', 'r') as configJson:
	config = json.load(configJson)


# mongo
mongoClient = MongoClient('mongodb://%s:%s@%s:%s' % (config['mongo']['user'], config['mongo']['pass'], config['mongo']['host'], config['mongo']['port']))
mongoDb = mongoClient[config['mongo']['database']]
appliedJobsCollection = mongoDb.applied_jobs
pendingJobsCollection = mongoDb.pending_jobs

def signal_handler(sig, frame):
	# print('You pressed Ctrl+C!')
	sys.exit(0)


"""
def train():
	totalJobs = ()
	for appliedJob in appliedJobsCollection.find({'processStatus': 'processed'}):
		totalJobs += (appliedJob,)
	textData = model.generateData(totalJobs)
	model.train(textData)

"""

def train():
	totalJobs = dict()
	for appliedJob in appliedJobsCollection.find({'processStatus': 'processed'}):
		if appliedJob['language'] not in totalJobs.keys():
			totalJobs[appliedJob['language']] = ()
		totalJobs[appliedJob['language']] += (appliedJob,)
	

	for lang in totalJobs:
		if len(totalJobs[lang]) <= 1:
			continue

		print(lang)

		textData = model.generateData(totalJobs[lang])
		model.train(textData, lang)

	"""
	totalJobs = ()
	for appliedJob in appliedJobsCollection.find({'processStatus': 'processed'}):
		totalJobs += (appliedJob,)
	# print(totalJobs)
	textData = model.generateData(totalJobs)
	print(textData)

	docs = dict()
	for job in totalJobs:
		if job['language'] not in docs.keys():
			docs[job['language']] = ()
		docs[job['language']] += (job,)

	textData = model.generateData(docs['english'])
	print(textData)
	"""

def getKeywords():
	totalJobs = ()
	for appliedJob in appliedJobsCollection.find({'processStatus': 'processed'}):
		totalJobs += (appliedJob,)

	docs = dict()
	for job in totalJobs:
		if job['language'] not in docs.keys():
			docs[job['language']] = []

		docs[job['language']].append(job['title'])
		# docs[job['language']].append(job['jobDescription'])

	keywords = model.getKeywords(docs)
	print(json.dumps(keywords))

def similar():
	totalJobs = ()
	for appliedJob in pendingJobsCollection.find({'processStatus': 'processed', 'simple': True}):
		similarity = model.similarity(appliedJob, appliedJob['language'])
		appliedJob['similarity'] = similarity
		totalJobs += (dict([('_id', str(appliedJob['_id'])), ('similarity', similarity)]),)
		# break
	print(json.dumps(totalJobs))
	
	



if __name__ == "__main__":
	signal.signal(signal.SIGINT, signal_handler)

	if sys.argv[1] == 'train':
		train()
	elif sys.argv[1] == 'keywords':
		getKeywords()
	elif sys.argv[1] == 'similar':
		similar()
	

	"""
	docs = dict()
	for job in totalJobs:
		if job['language'] not in docs.keys():
			docs[job['language']] = []

		docs[job['language']].append(job['title'])
		# docs[job['language']].append(job['jobDescription'])

	model.bow(docs)
	"""


