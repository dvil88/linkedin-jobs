import os
import re
import string
import json
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.linear_model import SGDClassifier
# from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics.pairwise import linear_kernel
from sklearn.metrics.pairwise import cosine_similarity


from keras.preprocessing.text import Tokenizer

from nltk import sent_tokenize
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer


import pickle


# from pathlib import Path
# from joblib import Parallel, delayed
# from functools import partial


import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS
from gensim.summarization import keywords


from TextRank4Keyword import TextRank4Keyword


# import spacy 
# from spacy.tokenizer import Tokenizer
# from spacy.util import minibatch


def appendData(data, batch_id, texts, output_dir):
	print(data)
	print(batch_id)
	print(texts)
	print(output_dir)

def generateData(textTuples):
	data = pd.DataFrame([], columns=['text'])
	"""
	batch_size = 1000
	n_jobs = 16
	
	partitions = minibatch(textTuples, size=batch_size)
	executor = Parallel(n_jobs=n_jobs, backend="multiprocessing", prefer="processes")
	do = delayed(partial(appendData, data))
	tasks = (do(i, batch, "./") for i, batch in enumerate(partitions))
	executor(tasks)
	return 
	"""

	for textTuple in textTuples:
		d = {"text": textTuple['jobDescription']}
		d2 = pd.Series(d)
		data = data.append(d2, ignore_index=True)
	return data

def train(data, lang):
	# TfidfVectorizer
	# tfidf = TfidfVectorizer(min_df = 10)
	vectorizer = TfidfVectorizer()

	tfidf_result = vectorizer.fit_transform(data["text"]).toarray()
	tfidf_df = pd.DataFrame(tfidf_result, columns = vectorizer.get_feature_names())
	tfidf_df.columns = ["word_" + str(x) for x in tfidf_df.columns]
	tfidf_df.index = data.index
	train_data_2 = pd.concat([data, tfidf_df], axis=1)

	docs_tfidf = vectorizer.fit_transform(data['text'])

	# TfidfTransformer
	tfidf_transformer = TfidfTransformer()
	train_result = tfidf_transformer.fit_transform(tfidf_result)


	"""
	# MultinomialNB
	clf = MultinomialNB().fit(train_result, data["type"])

	# SGDClassifier
	clf_svm = SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3, random_state=42).fit(train_result, data["type"])
	"""

	# print('dump')
	# return

	path = os.path.dirname(os.path.abspath(__file__))

	filename = path + '/model/vectorizer_' + lang + '.sav'
	pickle.dump(vectorizer, open(filename, 'wb'))

	filename = path + '/model/tfidf_df_' + lang + '.sav'
	pickle.dump(tfidf_df, open(filename, 'wb'))

	filename = path + '/model/docs_tfidf_' + lang + '.sav'
	pickle.dump(docs_tfidf, open(filename, 'wb'))

	# filename = 'model/tfidf_type.sav'
	# pickle.dump(tfidf_type, open(filename, 'wb'))

	filename = path + '/model/tfidf_transformer_' + lang + '.sav'
	pickle.dump(tfidf_transformer, open(filename, 'wb'))

	filename = path + '/model/train_result_' + lang + '.sav'
	pickle.dump(train_result, open(filename, 'wb'))

	"""
	filename = path + '/model/NB_model.sav'
	pickle.dump(clf, open(filename, 'wb'))

	filename = path + '/model/SVM_model.sav'
	pickle.dump(clf_svm, open(filename, 'wb'))
	"""

def getKeywords(data):
	t = Tokenizer()

	totalDocs = []
	for lang in data:
		stems = []

		docs = data[lang]
		for doc in docs:
			# split into words
			tokens = word_tokenize(doc)

			# convert to lower case
			tokens = [w.lower() for w in tokens]

			# remove punctuation from each word
			table = str.maketrans('', '', string.punctuation)
			stripped = [w.translate(table) for w in tokens]

			# remove remaining tokens that are not alphabetic
			words = [word for word in stripped if word.isalpha()]

			# filter out stop words
			stop_words = set(stopwords.words(lang))
			words = [w for w in words if not w in stop_words]


			totalDocs += words

	totalDocs = ' '.join(totalDocs)
	tr4k = TextRank4Keyword('english')
	tr4k.analyze(totalDocs, candidate_pos = ['NOUN', 'PROPN'], window_size=4, lower=False)
	keywords = tr4k.get_keywords(5)
	return keywords


def bow(data):
	t = Tokenizer()

	totalDocs = []
	for lang in data:
		print(' -> '+lang)
		stems = []

		docs = data[lang]
		# totalDocs += docs
		for doc in docs:
			# split into words
			tokens = word_tokenize(doc)

			# convert to lower case
			tokens = [w.lower() for w in tokens]

			# remove punctuation from each word
			table = str.maketrans('', '', string.punctuation)
			stripped = [w.translate(table) for w in tokens]

			# remove remaining tokens that are not alphabetic
			words = [word for word in stripped if word.isalpha()]

			# filter out stop words
			stop_words = set(stopwords.words(lang))
			words = [w for w in words if not w in stop_words]

			# print(words)
			# print(' '.join(words))
			totalDocs += words

			# stemming of words
			porter = PorterStemmer()
			stemmed = [porter.stem(word) for word in words]
			stems += stemmed

		mergedDocs = ' '.join(docs)

		# t.fit_on_texts(docs)
		# print(t.word_counts)


		tr4k = TextRank4Keyword(lang)
		if not tr4k:
			continue

		tr4k.analyze(mergedDocs, candidate_pos = ['NOUN', 'PROPN'], window_size=4, lower=False)
		# tr4k.get_keywords(10)

		# wordcloud = WordCloud(width = 1024, height = 768, random_state=1, background_color='black', colormap='Set2', collocations=False, stopwords = STOPWORDS).generate(mergedDocs)
		# wordcloud.to_file("wordcloud.png")

		# break

		"""
		t.fit_on_texts(docs)

		print(t.word_counts)
		# print(t.document_count)
		# print(t.word_index)
		# print(t.word_docs)

		encoded_docs = t.texts_to_matrix(docs, mode='count')
		# print(encoded_docs)

		return
		# wordcloud = WordCloud(width = 3000, height = 2000, random_state=1, background_color='black', colormap='Set2', collocations=False, stopwords = STOPWORDS).generate(docs)
		# Plot
		# plot_cloud(wordcloud)
		"""

	print("\n")
	print(totalDocs)
	totalDocs = ' '.join(totalDocs)
	tr4k = TextRank4Keyword('english')
	tr4k.analyze(totalDocs, candidate_pos = ['NOUN', 'PROPN'], window_size=4, lower=False)
	tr4k.get_keywords()

	

def detect(text):
	path = os.path.dirname(os.path.abspath(__file__))

	filename = path + '/model/tfidf_desc.sav'
	tfidf = pickle.load(open(filename, 'rb'))

	filename = path + '/model/tfidf_transformer.sav'
	tfidf_transformer = pickle.load(open(filename, 'rb'))

	# filename = path + '/model/NB_model.sav'
	# clf = pickle.load(open(filename, 'rb'))

	# filename2 = path + '/model/SVM_model.sav'
	# clf_svm = pickle.load(open(filename2, 'rb'))



	df = pd.DataFrame([text], columns=['text'])

	X_new_counts = tfidf.transform(df['text'].values.astype('U'))
	X_new_tfidf = tfidf_transformer.transform(X_new_counts)

	# predicted = clf.predict(X_new_tfidf)
	# predicted_svm = clf_svm.predict(X_new_tfidf)

	# return predicted[0]
	

def similarity(text, lang):
	path = os.path.dirname(os.path.abspath(__file__))


	filename = path + '/model/vectorizer_' + lang + '.sav'
	if not os.path.isfile(filename):
		return 0
		
	vectorizer = pickle.load(open(filename, 'rb'))

	filename = path + '/model/docs_tfidf_' + lang + '.sav'
	docs_tfidf = pickle.load(open(filename, 'rb'))


	data = generateData([text])
	text_tfidf = vectorizer.transform(data['text'])

	cosineSimilarities = cosine_similarity(text_tfidf, docs_tfidf).flatten()

	similarityAvg = (sum(cosineSimilarities) / len(cosineSimilarities)) * 100
	return similarityAvg
	