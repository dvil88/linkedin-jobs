const fs = require('fs');
const dateFormat = require('dateformat');

module.exports = {
	setFile: function(saveFile = false){
		if( saveFile ){
			module.exports.saveFile = fs.openSync(saveFile, 'a+')
			if( module.exports.saveFile === false ){
				module.exports.critical('Error opening log file');
			}
		}
	},
	setLevel: function(level){
		if( module.exports.saveFile){ level = 6; }
		module.exports.logLevel = level;
	},
	/*
	stdout: function(msg){
		if( strpos(msg, "\n") ){
			nl = preg_match_all("/\n/", msg);
			msg += (nl > 1 ? "\n" + '> ': '');
		} else if( msg[-1] == ']' ){
			msg += ' ';
		}

		console.log("\r\033[1;37m" + msg + "\033[0m");
	},
	*/

	info: function(){
		var msg = Array.from(arguments).join(' ');
		module.exports.showMsg(msg, 'INFO');
	},
	debug: function(){
		var msg = Array.from(arguments).join(' ');
		module.exports.showMsg(msg, 'DEBUG');
	},
	error: function(){
		var msg = Array.from(arguments).join(' ');
		module.exports.showMsg(msg, 'ERROR');
	},
	warn: function(){
		var msg = Array.from(arguments).join(' ');
		module.exports.showMsg(msg, 'WARNING');
	},
	critical: function(){
		var msg = Array.from(arguments).join(' ');
		module.exports.showMsg(msg, 'CRITICAL');
		process.exit();
	},
	showMsg: function(msg, level = false){
		var color;
		switch( level ){
			case 'INFO':		if( module.exports.logLevel < 1 ){ return; }
								color = '0;32';
								break;
			case 'DEBUG': 		if( module.exports.logLevel < 2 ){ return; }
								color = '0;34';
								break;
			case 'PAYLOAD':		color = '0;36';		break;
			case 'TRAFFIC OUT':	color = '0;35';		break;
			case 'TRAFFIC IN':	color = '45';		break;
			case 'ERROR':		color = '0;31';		break;
			case 'WARNING':		if( module.exports.logLevel < 1 ){ return; }
								color = '1;33';		break;
			case 'CRITICAL':	color = '41';		break;
			default:			color = '1;37';
		}

		if( module.exports.saveFile ){
			fs.writeSync(module.exports.saveFile, '[' + dateFormat(new Date(), "HH:MM:ss") + '] [' + level + '] ' + msg + "\n");
			// return;
		}
		console.log( String.fromCharCode(27) + '[' + color + 'm[' + dateFormat(new Date(), "HH:MM:ss") + '] [' + level + '] ' + msg + String.fromCharCode(27) + '[0m' );
	}
}